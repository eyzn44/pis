package game;

import java.util.Scanner;

/**
 * Created by oschi on 26.04.16.
 */
public class Main {
    private static int positions = 1;
    private static int wins = 0;

    public static void main(String[] args){

        Board board = new Board();
        /*
            Spielschleife
         */

        generateMoves(board);

        System.out.println("Positionen: " + positions);
        System.out.println("Gewinnpositionen: " + wins);

        while(true) {
            System.out.println("Spiel startet");

            //GUI gui = new GUI();
            //gui.setVisible(true);

            System.out.println(board.toString());

            Scanner scan = new Scanner(System.in);
            System.out.println("Warten auf Spieler Eingabe");

            String eingabe = scan.nextLine();

            while(!(eingabe.matches("[0-8]"))){
                System.out.println("Bitte geben Sie einen gültigen Wert ein (1-8):");
                eingabe = scan.nextLine();
            }

            if (board.checkPositionIsFree(Integer.parseInt(eingabe))){
                board.makeMove(Integer.parseInt(eingabe));
            } else {
                System.out.println("Feld: " + eingabe + " ist besetzt.");
            }

            /*
                Es macht erst Sinn nach dem 5. Zug zu kontrollieren, ob jemand gewonnen hat
             */
            //if(board.zug >= 5){
                int winner = board.threeInARow();
                if(winner != 0){
                    if(winner == 1){
                        System.out.println("Spieler 1 hat gewonnen");
                        System.out.println(board.toString());
                        break;
                    } else {
                        System.out.println("Spieler 2 hat gewonnen");
                        System.out.println(board.toString());
                        break;
                    }
                }
            //}
        }
    }

    private static void generateMoves(Board board){
        for(int pos = 0; pos <= board.listMoves(); pos++){
            board.makeMove(pos);
            positions += 1;
            if (board.threeInARow() != 0){
                wins += 1;
                board.undoMove();
                break;
            }
            generateMoves(board);
            board.undoMove();
        }
    }
}
