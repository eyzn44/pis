package game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by oschi on 24.04.16.
 */
public class GUI extends JFrame{

    LabelListener ll = new LabelListener();

    public GUI(){
        init();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    }

    private void init(){
        /*
            Aufbauen der GUI
         */
        this.setSize(500,500);

        JMenuBar bar = new JMenuBar();

        JMenu spielMenu = new JMenu("Spiel");

        JMenuItem reset = new JMenuItem("Reset");
        reset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            }
        });

        spielMenu.add(reset);

        bar.add(spielMenu);

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(3,3));
        panel.setBorder(BorderFactory.createLineBorder(Color.black, 5));
        this.setContentPane(panel);

        JLabel jLabel_0 = new JLabel();
        jLabel_0.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        jLabel_0.addMouseListener(ll);
        jLabel_0.setHorizontalAlignment(SwingConstants.CENTER);

        JLabel jLabel_1 = new JLabel();
        jLabel_1.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        jLabel_1.addMouseListener(ll);
        jLabel_1.setHorizontalAlignment(SwingConstants.CENTER);

        JLabel jLabel_2 = new JLabel();
        jLabel_2.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        jLabel_2.addMouseListener(ll);
        jLabel_2.setHorizontalAlignment(SwingConstants.CENTER);

        JLabel jLabel_3 = new JLabel();
        jLabel_3.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        jLabel_3.addMouseListener(ll);
        jLabel_3.setHorizontalAlignment(SwingConstants.CENTER);

        JLabel jLabel_4 = new JLabel();
        jLabel_4.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        jLabel_4.addMouseListener(ll);
        jLabel_4.setHorizontalAlignment(SwingConstants.CENTER);

        JLabel jLabel_5 = new JLabel();
        jLabel_5.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        jLabel_5.addMouseListener(ll);
        jLabel_5.setHorizontalAlignment(SwingConstants.CENTER);

        JLabel jLabel_6 = new JLabel();
        jLabel_6.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        jLabel_6.addMouseListener(ll);
        jLabel_6.setHorizontalAlignment(SwingConstants.CENTER);

        JLabel jLabel_7 = new JLabel();
        jLabel_7.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        jLabel_7.addMouseListener(ll);
        jLabel_7.setHorizontalAlignment(SwingConstants.CENTER);

        JLabel jLabel_8 = new JLabel();
        jLabel_8.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        jLabel_8.addMouseListener(ll);
        jLabel_8.setHorizontalAlignment(SwingConstants.CENTER);

        panel.add(jLabel_0);
        panel.add(jLabel_1);
        panel.add(jLabel_2);
        panel.add(jLabel_3);
        panel.add(jLabel_4);
        panel.add(jLabel_5);
        panel.add(jLabel_6);
        panel.add(jLabel_7);
        panel.add(jLabel_8);


        this.setJMenuBar(bar);
    }

    class LabelListener implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            ((JLabel)e.getSource()).setText("x");
        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }
}
